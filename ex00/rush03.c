/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush03.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jorgonza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/09 14:50:26 by jorgonza          #+#    #+#             */
/*   Updated: 2022/07/09 16:10:04 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print_bot(int x)
{
	ft_putchar('A');
	while (x > 2)
	{
		ft_putchar('B');
		x--;
	}
	if (x == 2)
		ft_putchar('C');
	ft_putchar('\n');
}

void	print_top(int x)
{
	ft_putchar('A');
	while (x > 2)
	{
		ft_putchar('B');
		x--;
	}
	if (x == 2)
		ft_putchar('C');
	ft_putchar('\n');
}

void	rush(int x, int y)
{	
	int	temp_x;

	temp_x = x;
	print_top(x);
	while (y > 2)
	{
		ft_putchar('B');
		while (x > 2)
		{
			ft_putchar(' ');
			x--;
		}
		if (x == 2)
			ft_putchar('B');
		ft_putchar('\n');
		x = temp_x;
		y--;
	}
	if (y == 2)
		print_bot(temp_x);
}
