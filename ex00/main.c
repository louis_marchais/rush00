/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jorgonza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/09 11:01:46 by jorgonza          #+#    #+#             */
/*   Updated: 2022/07/09 18:24:50 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	rush(int x, int y);

int	ft_atoi(char *str)
{
	int	nbr;
	int	i;

	nbr = 0;
	i = 0;
	if (str[i] == '-')
		return (0);
	while (str[i] != '\0')
	{
		if (nbr >= 214748364 && str[i] > '7')
			return (0);
		if (nbr >= 214748365)
			return (0);
		if (str[i] >= '0' && str[i] <= '9')
		{
			nbr = nbr * 10 + str[i] - 48;
			i++;
		}
		else
		{
			return (0);
		}	
	}
	return (nbr);
}

int	main(int argc, char *argv[])
{
	if (argc == 3)
	{
		if (ft_atoi(argv[1]) == 0 || ft_atoi(argv[2]) == 0)
			return (0);
		else
			rush(ft_atoi(argv[1]), ft_atoi(argv[2]));
	}
	return (0);
}
