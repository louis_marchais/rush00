/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jorgonza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/09 09:56:28 by jorgonza          #+#    #+#             */
/*   Updated: 2022/07/09 16:07:31 by lmarchai         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print_bot(int x)
{
	ft_putchar('o');
	while (x > 2)
	{
		ft_putchar('-');
		x--;
	}
	if (x == 2)
		ft_putchar('o');
	ft_putchar('\n');
}

void	print_top(int x)
{
	ft_putchar('o');
	while (x > 2)
	{
		ft_putchar('-');
		x--;
	}
	if (x == 2)
		ft_putchar('o');
	ft_putchar('\n');
}

void	rush(int x, int y)
{	
	int	temp_x;

	temp_x = x;
	print_top(x);
	while (y > 2)
	{
		ft_putchar('|');
		while (x > 2)
		{
			ft_putchar(' ');
			x--;
		}
		if (x == 2)
			ft_putchar('|');
		ft_putchar('\n');
		x = temp_x;
		y--;
	}
	if (y == 2)
		print_bot(temp_x);
}
